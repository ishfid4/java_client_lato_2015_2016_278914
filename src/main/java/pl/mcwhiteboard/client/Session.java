package pl.mcwhiteboard.client;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import pl.mcwhiteboard.client.model.Room;
import pl.mcwhiteboard.client.model.User;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Created by ishfid on 4/16/16.
 */
public class Session {
    public User user;
    private Socket socket;
    private Connection connection;
    private BiConsumer<Boolean, String> loggedInCallback = null;
    private BiConsumer<Boolean, String> signedUpCallback = null;
    private Consumer<Boolean> addedRoomCallback = null;
    private Consumer<Boolean> joinedRoomCallback = null;
    private Consumer<Boolean> updatedPasswordCallback = null;
    private Consumer<Boolean> updatedNicknameCallback = null;
    private Consumer<Boolean> updatedNickNpassCallback = null;
    private Consumer<Boolean> canvasSendCallback = null;

    public ObservableList<Room> rooms = FXCollections.observableArrayList();
    public ObservableList<String> chatLines = FXCollections.observableArrayList();
    public String imageCanvas = null;
    public Boolean canvasUpdate = false;

    public Session(Socket socket) throws IOException {
        user = new User();
        this.socket = socket;
        connection = new Connection(socket);


        SocketReader sReader = new SocketReader(socket, this);
        new Thread(sReader).start();
    }

    public void login(String username, String pass, BiConsumer<Boolean, String> callback) throws IOException {
        this.loggedInCallback = callback;
        String msg = "USER UserLogin" + " " + username + " " + pass;
        connection.sendCommand(msg);
    }

    public void onLogin(Boolean success, String msg) {
        if(loggedInCallback != null) {
            loggedInCallback.accept(success, msg);
            loggedInCallback = null;
        }
    }

    public void signUp(String username, String pass, String nickname, BiConsumer<Boolean, String> callback) throws IOException{
        this.signedUpCallback = callback;
        String msg = "USER UserAdd" + " " + username + " " + pass + " " + nickname;
        connection.sendCommand(msg);
    }

    public void onSingUp(Boolean success, String msg){
        if(signedUpCallback != null){
            signedUpCallback.accept(success, msg);
            signedUpCallback = null;
        }
    }

    public void updateRoomList() throws IOException {
        rooms.removeAll();
        connection.sendCommand("ROOM RoomList");
    }

    public void addRoom(String name, Consumer<Boolean> callback) throws IOException {
        this.addedRoomCallback = callback;
        connection.sendCommand("ROOM RoomCreate " + name);
    }

    public void onAddRoom(Boolean success){
        if(addedRoomCallback != null){
            addedRoomCallback.accept(success);
            addedRoomCallback = null;
        }
    }

    public void joinRoom(String id, Consumer<Boolean> callback) throws IOException{
        this.joinedRoomCallback = callback;
        connection.sendCommand("ROOM RoomJoin " + id);
    }

    public void onJoin(Boolean success){
        if(joinedRoomCallback != null){
            joinedRoomCallback.accept(success);
            joinedRoomCallback = null;
        }
    }

    public void updateUserPassword(String pass, Consumer<Boolean> callback) throws IOException{
        this.updatedPasswordCallback = callback;
        connection.sendCommand("USER UserUpdate EditPassword " + pass);
    }


    public void updateUserNickname(String nick, Consumer<Boolean> callback) throws IOException{
        this.updatedNicknameCallback = callback;
        connection.sendCommand("USER UserUpdate EditNick " + nick);
    }

    public void updateUserPassNnick(String pass, String nick, Consumer<Boolean> callback) throws IOException{
        this.updatedNickNpassCallback = callback;
        connection.sendCommand("USER UserUpdate EditNick " + nick);
        connection.sendCommand("USER UserUpdate EditPassword " + pass);
    }

    public void onUserUpadate(Boolean success){
        if(updatedNicknameCallback != null){
            updatedNicknameCallback.accept(success);
            updatedNicknameCallback = null;
        }
        if(updatedPasswordCallback != null){
            updatedPasswordCallback.accept(success);
            updatedPasswordCallback = null;
        }
        if(updatedNickNpassCallback != null){
            updatedNickNpassCallback.accept(success);
            updatedNickNpassCallback = null;
        }
    }

    public void chatMessageSend(String msg) throws IOException {
        connection.sendCommand("CHAT ChatSend " + msg);
    }

    public void canvasSend(String imageString, Consumer<Boolean> callback) throws IOException {
        this.canvasSendCallback = callback;
        connection.sendCommand("CANVAS Update " + imageString);
    }

    public void onCavasSend(Boolean success){
        if(canvasSendCallback != null){
            canvasSendCallback.accept(success);
            canvasSendCallback = null;
        }
    }
}
