package pl.mcwhiteboard.client.commands;

import pl.mcwhiteboard.client.Session;

/**
 * Created by ishfid on 23.04.16.
 */
public class Success implements Command {
    private Session session;
    private String msg;
    public Success(String msg, Session session) {
        this.session = session;
        this.msg = msg;
    }

    @Override
    public void execute(){
        String[] tokens = msg.split(" ",2);
        if(msg.equals("SIGNUP"))
            session.onSingUp(true, msg);
        if(tokens[0].equals("LOGIN"))
            session.onLogin(true, tokens[1]);
        if(tokens[0].equals("ROOMADD"))
            session.onAddRoom(true);
        if(tokens[0].equals("ROOMJOIN"))
            session.onJoin(true);
        if(tokens[0].equals("USERNICKCHANGE") || tokens[0].equals("USERPASSCHANGE"))
            session.onUserUpadate(true);
        if(tokens[0].equals("ImageUpdated"))
            session.onCavasSend(true);
    }
}
