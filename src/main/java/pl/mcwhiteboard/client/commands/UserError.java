package pl.mcwhiteboard.client.commands;

import pl.mcwhiteboard.client.Session;

import java.util.function.BiConsumer;

/**
 * Created by ishfid on 4/18/16.
 */
public class UserError implements Command{
    private String error;
    private Session session;

    public UserError(String error, Session session) {
        this.error = error;
        this.session = session;
    }

    @Override
    public void execute(){
        session.onLogin(false, error);
    }
}
