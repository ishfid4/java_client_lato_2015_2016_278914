package pl.mcwhiteboard.client.commands;

import javafx.application.Platform;
import pl.mcwhiteboard.client.Session;

/**
 * Created by ishfid on 28.05.16.
 */
public class CanvasUpdate implements Command {
    private String imageString;
    private Session session;

    public CanvasUpdate(String msg, Session session) {
        String[] tokens = msg.split(" ",2);
        if(tokens[0].equals("Update")) {
            this.imageString = tokens[1];
            this.session = session;

        }
    }

    @Override
    public void execute(){
        Platform.runLater(() -> {
            session.imageCanvas = imageString;
            session.canvasUpdate = true;

        });
    }
}

