package pl.mcwhiteboard.client.commands;

/**
 * Created by ishfid on 4/18/16.
 */
public interface Command {
    void execute();
}
