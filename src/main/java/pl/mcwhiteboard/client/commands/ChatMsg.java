package pl.mcwhiteboard.client.commands;

import javafx.application.Platform;
import pl.mcwhiteboard.client.Session;

import java.io.IOException;

/**
 * Created by ishfid on 04.05.16.
 */
public class ChatMsg implements Command {
    private String msg;
    private Session session;

    public ChatMsg(String msg, Session session) {
        this.msg = msg;
        this.session = session;
    }

    @Override
    public void execute(){
        Platform.runLater(() -> session.chatLines.add(msg));
    }
}
