package pl.mcwhiteboard.client.commands;

import pl.mcwhiteboard.client.Session;
import pl.mcwhiteboard.client.model.Room;

/**
 * Created by ishfid on 24.04.16.
 */
public class RoomGet implements Command {
    private Session session;
    private String msg;

    public RoomGet(String msg, Session session) {
        this.session = session;
        this.msg = msg;
    }

    @Override
    public void execute(){
        String[] tokens = msg.split(" ",2);
        session.rooms.add(new Room(tokens[0],tokens[1]));
    }
}


