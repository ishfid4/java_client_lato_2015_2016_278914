package pl.mcwhiteboard.client.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pl.mcwhiteboard.client.Session;

import java.io.IOException;

/**
 * Created by ishfid on 4/15/16.
 */
public class SignupController {
    private Session session;

    public SignupController() {
    }

    public void setSession(Session session) {
        this.session = session;
    }

    @FXML
    private TextField loginTextField;
    @FXML
    private TextField nicknameTextField;
    @FXML
    private PasswordField passwordTextField;
    @FXML
    private PasswordField repasswordTextField;
    @FXML
    private Label singupLabelStatus;
    @FXML
    private Button buttonCancel;
    @FXML

    public void signupClicked() throws IOException{
        boolean err = false;
        if(loginTextField.getText().isEmpty()){
            singupLabelStatus.setText("Enter login");
            err = true;
        }
        if(nicknameTextField.getText().isEmpty()){
            singupLabelStatus.setText("Ener nickname");
            err = true;
        }
        if(passwordTextField.getText().isEmpty()){
            singupLabelStatus.setText("Enter password");
            err = true;
        }
        if(repasswordTextField.getText().isEmpty()){
            singupLabelStatus.setText("Retype password");
            err = true;
        }
        if(!passwordTextField.getText().equals(repasswordTextField.getText())){
            singupLabelStatus.setText("Password missmatch");
            err = true;
        }

        if(!err)
            session.signUp(loginTextField.getText(),passwordTextField.getText(),nicknameTextField.getText(), this::signedUp) ;
    }

    private void signedUp(Boolean success, String msg) {
        if(success){
            Platform.runLater(() -> {
                try {
                    cancelClicked();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    @FXML
    public void cancelClicked() throws IOException {
        Stage stage;
        stage = (Stage) buttonCancel.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/loginWindow.fxml"));
        Parent root = loader.load();
        LoginController controller = loader.getController();
        controller.setSession(session);


        //create a new scene with root and set the stage
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

}
