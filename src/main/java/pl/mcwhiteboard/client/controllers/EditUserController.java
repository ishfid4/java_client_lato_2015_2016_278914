package pl.mcwhiteboard.client.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import pl.mcwhiteboard.client.Session;

import java.io.IOException;

/**
 * Created by ishfid on 01.05.16.
 */
public class EditUserController {
    private Session session;

    public void setSession(Session session) {
        this.session = session;
    }

    @FXML
    private Button cancelButton;
    @FXML
    private PasswordField passwordTextField;
    @FXML
    private PasswordField repassTextField;
    @FXML
    private TextField nickTextField;
    @FXML
    private CheckBox passwordCheckBox;
    @FXML
    private CheckBox nicknameCheckBox;
    @FXML
    private Label statusLabel;

    @FXML
    public void okClicked() throws IOException {
        if(passwordCheckBox.isSelected() && nicknameCheckBox.isSelected()){
            if(passwordTextField.getText().isEmpty() || repassTextField.getText().isEmpty() ||
                    (!passwordTextField.getText().equals(repassTextField.getText())))
                statusLabel.setText("Insert proper passowrd");
            else {
                if (nickTextField.getText().isEmpty())
                    statusLabel.setText("Insert Nickname");
                else {
                    session.updateUserPassNnick(passwordTextField.getText(), nickTextField.getText(), this::updatedUser);
                }
            }
        }
        if(passwordCheckBox.isSelected()){
            if(passwordTextField.getText().isEmpty() || repassTextField.getText().isEmpty() ||
                    (!passwordTextField.getText().equals(repassTextField.getText())))
                statusLabel.setText("Insert proper passowrd");
            else{
               session.updateUserPassword(passwordTextField.getText(), this::updatedUser);
            }
        }
        if(nicknameCheckBox.isSelected()){
            if(nickTextField.getText().isEmpty())
                statusLabel.setText("Insert Nickname");
            else {
                session.updateUserNickname(nickTextField.getText(), this::updatedUser);
            }
        }
        statusLabel.setText("Chose what you want to change");
    }

    private void updatedUser(Boolean success){
        if(success)
            cancelClicked();
    }

    @FXML
    public void cancelClicked(){
        Platform.runLater(() -> {
            Stage stage;
            stage = (Stage) cancelButton.getScene().getWindow();

            stage.close();
        });
    }
}
