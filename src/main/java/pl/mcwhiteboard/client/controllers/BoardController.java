package pl.mcwhiteboard.client.controllers;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import pl.mcwhiteboard.client.Session;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Base64;

/**
 * Created by ishfid on 01.05.16.
 */
public class BoardController {
    private Session session;
    private GraphicsContext graphicsContext;

    @FXML
    private TextField chatTextField;
    @FXML
    private ListView<String> chatListView;
    @FXML
    private Canvas drawingCanvasBackground;
    @FXML
    private Canvas drawingCanvasCurrentDrawing;

    public void setSession(Session session) {
        this.session = session;
        this.session.chatLines.add("Weclome: " + session.user.getNickname());
        chatListView.setItems(session.chatLines);
        graphicsContext = drawingCanvasCurrentDrawing.getGraphicsContext2D();
        initDraw(graphicsContext);
    }

    @FXML
    public void sendClicked() throws IOException {
        if(!chatTextField.getText().isEmpty()){
            session.chatLines.add(session.user.getNickname() + ": " + chatTextField.getText());
            session.chatMessageSend(session.user.getNickname() + ": " + chatTextField.getText());
        }
    }

    @FXML
    public void startDrawing(MouseEvent mouseEvent) throws IOException {
        if (session.imageCanvas != null)
            decodeAndUpdateCanvas();

        graphicsContext.beginPath();
        graphicsContext.lineTo(mouseEvent.getX(), mouseEvent.getY());
        graphicsContext.stroke();
    }

    //dunno how to remove line between drawing a curve
    @FXML
    public void drawingTrace(MouseEvent mouseEvent){
        graphicsContext.lineTo(mouseEvent.getX(), mouseEvent.getY());
        graphicsContext.stroke();
    }

    @FXML
    public void endDrawing(MouseEvent mouseEvent) throws IOException{
        graphicsContext.closePath();
        drawingCanvasBackground = graphicsContext.getCanvas();

        encodeCanvasAndSend();
    }

    private void encodeCanvasAndSend() throws IOException{
        SnapshotParameters snapshotParameters = new SnapshotParameters();
        WritableImage wi = new WritableImage(800,800);
        WritableImage snapshot = drawingCanvasCurrentDrawing.snapshot(snapshotParameters, wi);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(snapshot, null), "png", byteArrayOutputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Base64.Encoder encoder = Base64.getEncoder();
        //session.imageCanvas = encoder.encodeToString(byteArrayOutputStream.toByteArray());
        session.canvasSend(encoder.encodeToString(byteArrayOutputStream.toByteArray()),this::sendedCanvas);
    }

    private void sendedCanvas(Boolean success) {
        if(success){
            Platform.runLater(() -> {
//                SnapshotParameters snapshotParameters = new SnapshotParameters();
//                WritableImage wi = new WritableImage(800, 800);
//                WritableImage snapshot = drawingCanvasCurrentDrawing.snapshot(snapshotParameters, wi);
//
//                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//                try {
//                    ImageIO.write(SwingFXUtils.fromFXImage(snapshot, null), "png", byteArrayOutputStream);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//                Base64.Encoder encoder = Base64.getEncoder();
//                session.imageCanvas = encoder.encodeToString(byteArrayOutputStream.toByteArray());
                try {
                    decodeAndUpdateCanvas();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public void decodeAndUpdateCanvas() throws IOException{
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] imageData = decoder.decode(session.imageCanvas);
        InputStream inputStream = new ByteArrayInputStream(imageData);

        WritableImage wi = new WritableImage(800,800);
        BufferedImage bufferedImage = ImageIO.read(inputStream);
        Image image = SwingFXUtils.toFXImage(bufferedImage,wi);

        graphicsContext.drawImage(image, 0, 0);
    }

    private void initDraw(GraphicsContext gc){
        gc.setStroke(Color.BLACK);
        gc.setLineWidth(1);
    }

    @FXML
    public void takeScreen(){
        SnapshotParameters snapshotParameters = new SnapshotParameters();
        WritableImage wi = new WritableImage(800,800);
        WritableImage snapshot = drawingCanvasCurrentDrawing.snapshot(snapshotParameters, wi);

        File out = new File("screen.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(snapshot, null), "png", out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
