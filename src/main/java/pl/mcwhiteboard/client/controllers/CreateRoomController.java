package pl.mcwhiteboard.client.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pl.mcwhiteboard.client.Session;

import java.io.IOException;

/**
 * Created by ishfid on 25.04.16.
 */
public class CreateRoomController {
    private Session session;

    public void setSession(Session session) {
        this.session = session;
    }
    @FXML
    private TextField roomNameTextField;
    @FXML
    private Label statusLabel;
    @FXML
    private Button cancelButton;
    @FXML
    private Button createButton;

    @FXML
    public void cancelClicked(){
        Platform.runLater(() -> {
            Stage stage;
            stage = (Stage) createButton.getScene().getWindow();

            stage.close();
        });
    }

    @FXML
    public void createClicked() throws IOException {
        if(roomNameTextField.getText().isEmpty())
            statusLabel.setText("Insert name");
        else{
            session.addRoom(roomNameTextField.getText(), this::addedRoom);
            session.updateRoomList();
        }
    }

    private void addedRoom(Boolean success){
        if(success){
           cancelClicked();
        }
    }

}
