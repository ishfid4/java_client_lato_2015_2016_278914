package pl.mcwhiteboard.client.controllers;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pl.mcwhiteboard.client.Session;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;

/**
 * Created by ishfid on 4/15/16.
 */
public class LoginController {
    private Session session = null;

    public void setSession(Session session) {
        this.session = session;
    }

    @FXML
    private Button buttonLogin;
    @FXML
    private Button buttonSignup;
    @FXML
    private Label loginLabelStatus;
    @FXML
    private TextField loginTextField;
    @FXML
    private PasswordField passwordField;

    @FXML
    public void loginClicked() throws IOException {
        session.user.setLogin(loginTextField.getText());
        session.user.setPassword(passwordField.getText());
        session.login(loginTextField.getText(),passwordField.getText(), this::loggedIn );
    }

    private void loggedIn(Boolean success, String messages) {
        //ten szajs zostanie wykonany przy nast obiegu petli ui
        if(success){
            session.user.setNickname(messages);
            Platform.runLater(() -> {
                Stage stage;
                stage = (Stage) buttonLogin.getScene().getWindow();

                FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/roomWindow.fxml"));
                Parent root = null;
                try {
                    session.updateRoomList();

                    root = loader.load();
                    RoomController controller = loader.getController();
                    controller.setSession(session);

                    Scene scene = new Scene(root);
                    stage.setScene(scene);
                    stage.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
        else
            Platform.runLater(() -> loginLabelStatus.setText(messages));
    }

    @FXML
    public void signupClicked(ActionEvent event) throws IOException {
        Stage stage;
        stage = (Stage) buttonSignup.getScene().getWindow();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/signupWindow.fxml"));
        Parent root = loader.load();
        SignupController controller = loader.getController();
        controller.setSession(session);


        //create a new scene with root and set the stage
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
}
