package pl.mcwhiteboard.client.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import pl.mcwhiteboard.client.Session;
import pl.mcwhiteboard.client.model.Room;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by ishfid on 23.04.16.
 */
public class RoomController implements Initializable{
    private Session session;

    public void setSession(Session session) {
        this.session = session;
        tableView.setItems(session.rooms);
    }

    @FXML
    private TableView<Room> tableView;
    @FXML
    private TableColumn<Room,String> idColumn;
    @FXML
    private TableColumn<Room,String> nameColumn;
    @FXML
    private Button buttonJoin;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        idColumn.setCellValueFactory(new PropertyValueFactory<Room, String>("id"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<Room, String>("name"));
    }

    @FXML
    public void joinClicked() throws IOException {
        session.joinRoom(tableView.getFocusModel().getFocusedItem().getId(), this::joined);
    }

    private void joined(Boolean success){
        if(success){
            Platform.runLater(() -> {
                Stage stage;
                stage = (Stage) buttonJoin.getScene().getWindow();

                FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/boardWindow.fxml"));
                Parent root = null;
                try {
                    root = loader.load();
                    BoardController controller = loader.getController();
                    controller.setSession(session);

                    Scene scene = new Scene(root);
                    stage.setScene(scene);
                    stage.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    @FXML
    public void createClicked(){
        Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/createRoomAlert.fxml"));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        dialogStage.setScene(new Scene(root));
        CreateRoomController controller = loader.getController();
        controller.setSession(session);
        dialogStage.show();
    }

    @FXML
    public void editCliked(){
        Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/editUserAlert.fxml"));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        dialogStage.setScene(new Scene(root));
        EditUserController controller = loader.getController();
        controller.setSession(session);
        dialogStage.show();
    }
}
