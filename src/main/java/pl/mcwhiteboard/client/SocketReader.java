package pl.mcwhiteboard.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.function.BiConsumer;

/**
 * Created by ishfid on 4/16/16.
 */
public class SocketReader implements Runnable{
    private Scanner in;
    private PrintStream out;
    private Socket socket;
    private OutputStream outputStream;
    private Session session;

    private CommandHandler commandHandler;

    public SocketReader(Socket socket, Session session) throws IOException {
        this(socket.getInputStream(), socket.getOutputStream());
        this.socket = socket;
        this.outputStream = socket.getOutputStream();
        this.session = session;
    }

    public SocketReader(InputStream input, OutputStream output) {
        in = new Scanner(input);
        out = new PrintStream(output);
    }

    private void msg(String msg) {
        System.out.println("Client: " + msg);
    }

    public void run() {
        msg("new connetions");
        this.commandHandler = new CommandHandler();
        while((!Thread.currentThread().isInterrupted()) && in.hasNextLine()) {
            String line = in.nextLine();
            msg(line);
            //Processing commands
            commandHandler.handleCommand(line, session);
        }
        try {
            out.close();
            socket.close();
        } catch (IOException e){
            e.printStackTrace();
        }
        msg("connetion closed");
    }
}

