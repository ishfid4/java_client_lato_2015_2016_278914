package pl.mcwhiteboard.client;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

/**
 * Created by ishfid on 4/18/16.
 */
public class Connection {
    private Socket socket;

    public Connection(Socket socket) {
        this.socket = socket;
    }

    public void sendCommand(String msg) throws IOException {
        PrintStream out = new PrintStream(socket.getOutputStream());
        out.println(msg);
    }
}
