package pl.mcwhiteboard.client.model;

/**
 * Created by ishfid on 4/18/16.
 */
public class User {
    private String login;
    private String password;
    private String nickname;

    public User(){ }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
