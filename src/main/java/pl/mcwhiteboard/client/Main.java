package pl.mcwhiteboard.client;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import pl.mcwhiteboard.client.controllers.LoginController;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Created by ishfid on 4/10/16.
 */
public class Main extends Application {
    public static void main(String[] args) throws IOException {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Socket socket =  new Socket();
        socket.connect(new InetSocketAddress("127.0.0.1", 1512));

        Session session = new Session(socket);


        FXMLLoader loader = new FXMLLoader(getClass().getResource("/layout/loginWindow.fxml"));
        Parent root = loader.load();
        LoginController controller = loader.getController();
        controller.setSession(session);
        Scene sceneLogin = new Scene(root);

        stage.setTitle("WhiteBoard");
        stage.setScene(sceneLogin);
        stage.show();
    }
}
