package pl.mcwhiteboard.client;

import pl.mcwhiteboard.client.commands.*;

import java.util.function.BiConsumer;

/**
 * Created by ishfid on 4/18/16.
 */
public class CommandHandler {
    private UserError userError;
    private Success success;
    private RoomGet roomGet;
    private ChatMsg chatMsg;
    private CanvasUpdate canvasUpdate;

    public CommandHandler() {
    }

    public void handleCommand(String msg, Session session){
        String[] tokens = msg.split(" ",2);

        if(tokens[0].equals("UserError")) {
            userError = new UserError(tokens[1], session);
            userError.execute();
        }

        if(tokens[0].equals("SUCCESS")) {
            success = new Success(tokens[1], session);
            success.execute();
        }

        if(tokens[0].equals("ROOMGET")){
            roomGet = new RoomGet(tokens[1], session);
            roomGet.execute();
        }

        if(tokens[0].equals("CHATMSG")){
            chatMsg = new ChatMsg(tokens[1], session);
            chatMsg.execute();
        }

        if(tokens[0].equals("CANVAS")){
            canvasUpdate = new CanvasUpdate(tokens[1], session);
            canvasUpdate.execute();
        }
    }
}
